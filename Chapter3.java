public class Chapter3 {
	
//--------------------- Question 2 ------------------------//
	// LeetCode
	/*
		class MinStack {
		    Stack<Integer> stack;
		    Stack<Integer> minStack;
		    
		    public MinStack() {
		        stack = new Stack<Integer>();
		        minStack = new Stack<Integer>();
		    }
		    
		    public void push(int x) {
		        stack.push(x);
		        if (minStack.isEmpty() || x <= minStack.peek()) {
		            minStack.push(x);
		        }
		    }

		    public void pop() {
		        int res = stack.pop();
		        if (!minStack.isEmpty() && res == minStack.peek()) {
		            minStack.pop();
		        }
		    }

		    public int top() {
		        return stack.peek();
		    }

		    public int getMin() {
		        return minStack.peek();
		    }
		}

	*/
//--------------------- Question 3 ------------------------//
	
		class SetOfStacks {
			ArrayList<Stack<Integer>> set;
			int current;
			public SetOfStacks(int size) {
				set = new ArrayList<Stack<Integer>>(size);
				for (int i = 0; i < size; i++) {
					set.add(new Stack<Integer>());
				}
				current = 0;
			}

			public Integer pop() {
				while (set.get(current).size() == 0) {
					current--;
				}	
				if (current < 0) return Integer.MIN_VALUE;

				return set.get(current).pop();
			}

			public void push(int value) {
				while (set.get(current).size() == 0) {
					current--;
				}
				if (current < 0) current ++;
				set.get(current).push(value);
			}

			public Integer popAt(int index) {
				if (index >= set.size()) return Integer.MIN_VALUE;
				if (set.get(index).size() == 0) return Integer.MIN_VALUE;
				return set.get(index).pop();
			}
		}
	





	// Support Functions
	public static void P(String s) {
		System.out.println(s);
	}

	public static ListNode create(int length, int seed) {
		Random rand = new Random(seed);
		ListNode dummy = new ListNode(0);
		ListNode p = dummy;

		for (int i  = 0; i < length; i++) {
			p.next = new ListNode(rand.nextInt(10) + 1);
			p = p.next;
		}
		return dummy.next;
	}
}