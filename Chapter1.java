import java.util.*;

public class Chapter1 {
//--------------------- Question 1 ------------------------//
	public static boolean uniqueCharacters1(String s) {
		boolean map[] = new boolean[256];

		char chars[] = s.toCharArray();

		for (char c: chars) {
			if (!map[c]) {
				map[c] = true;
			} else 
				return false;
		}
		return true;
	}

	public static boolean uniqueCharacters2(String s) {
		HashSet<Character> set = new HashSet<Character>();
		char chars[] = s.toCharArray();
		for (char c: chars) {
			if (!set.contains(c)) {
				set.add(c);
			} else 
			    return false;
		}
		return true;
	}
	// Only for lower of upper cases
	public static boolean uniqueCharacters3(String s) {
		int counter = 0;

		char chars[] = s.toCharArray();

		for (char c: chars) {
			int bit = 1 << (c - 'a');
			if ((bit & counter) == 0) {
				counter |= bit;
			} else 
				return false;
		}

		return true;
	}
//---------------------Question 2 ------------------------//
	public static String reverseString(String s) {
		StringBuffer sb = new StringBuffer();
		char chars[] = s.toCharArray();

		for (char c: chars)
			sb.insert(0, c);
		return sb.toString();
	}

	public static String reverseString2(String s) {
		if (s.length() < 2) return s;
		char chars[] = s.toCharArray();

		int p = 0;
		int q = s.length() - 1;

		while (p < q) {
			char temp = chars[p];
			chars[p] = chars[q];
			chars[q] = temp;
			p++;
			q--;
		}
		return new String(chars);
	}

//---------------------Question 3 ------------------------//
	// Complexity: O(N LOGN) O(N)
	public static boolean isPermutation(String s1, String s2) {
		if (s1.length() != s2.length()) return false;

		char c1[] = s1.toCharArray();
		char c2[] = s2.toCharArray();
		Arrays.sort(c1);
		Arrays.sort(c2);
		// Here we also could just 
		return (new String(c1)).equals(new String(c2));
		// for (int i = 0; i < s1.length(); i++) {
		// 	if (c1[i] != c2[i]) return false;
		// }
		// return true;
	}
	
	// Complexity: O(N) O(N)
	// Limit: ASCII code only
	public static boolean isPermutation2(String s1, String s2) {
		if (s1.length() != s2.length()) return false;

		int table[] = new int[256];

		char c1[] = s1.toCharArray();
		char c2[] = s2.toCharArray();

		for (int i = 0; i < s1.length(); i++) {
			table[c1[i]]--;
			table[c2[i]]++;
		}
		for (int i = 0; i < 256; i++) {
			if (table[i] != 0) return false;
		}
		return true;
	}
//---------------------Question 4 ------------------------//
	// length is the length of the original string
	// str.length cound be seen as infinite large
	// This technique could be used in multiple places, like mergesort
	// IN PLACE
	public static void replace (char[] str, int length) {
		int count = 0;

		for (int i = 0; i < length ; i++) {
			if (str[i] == ' ') count++;
		}
		int real_length = count * 2 +  length;
		int j = real_length - 1;

		for (int i = length - 1; i >= 0; i--) {
			if (str[i] != ' ') {
				str[j] = str[i];
				j--;
			} else {
				str[j] = '0';
				str[j - 1] = '2';
				str[j - 2] = '%';
				j -= 3;
			}
		}
	}


//---------------------Question 5 ------------------------//

	public static String compress(String s) {
		if (s.length() < 2) return s;
		StringBuffer sb = new StringBuffer();
		char chars[] = s.toCharArray();
		char c = chars[0];
		int count = 1;

		for (int i  = 1; i < s.length(); i++) {
			if (chars[i] != c) {
				sb.append(c);
				sb.append(count);
				c = chars[i];		
				count = 1;
			} else {
				count++;
			}
		}
		sb.append(c);
		sb.append(count);
		return sb.length() < s.length()?sb.toString():s;
	}


//---------------------Question 6 ------------------------//
	// Easy quesiton
	public static int[][] rotate(int[][] image) {
		if (image.length < 2) return image;

		int temp = 0;
		int N = image.length;
		for (int i = 0; i < (image.length)/2; i++) {
			for (int j = 0; j < (image.length + 1)/2; j++) {
				temp = image[i][j];
				image[i][j] = image[N - 1 - j][i];
				image[N - 1 - j][i] = image[N - 1 - i][N - 1 - j];
				image[N - 1 - i][N - 1 -j] = image[j][N - 1 - i];
				image[j][N - 1 - i] = temp;
			}
		}
		return image;
	}

//---------------------Question 7 ------------------------//
	public static void clean(int[][] input) {
		int N = input.length;
		int M = input[0].length;

		boolean col[] = new boolean[M];
		boolean row[] = new boolean[N];

		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				if (input[i][j] == 0) {
					row[i] = true;
					col[j] = true;
				}
			}
		}
		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++) {
				if (col[j] || row[i])
					input[i][j] = 0;
			}
		}
	}

//--------------------- Question 8 ------------------------//
	public static boolean isSubstring(String s1, String s2) {
		if (s1.length() > s2.length()) return false;
		int l1 = s1.length();
		int l2 = s2.length();

		for (int i = 0; i < l2 - l1 + 1; i++) {
			if (s2.substring(i, i + l1).equals(s1))
				return true;
		}
		return false;
	}

	public static boolean isStringRotate(String s1, String s2) {
		if (s1.length() != s2.length()) return false;

		return isSubstring(s2, s1+s1);
	}


	public static void main(String[] args) {
		String s = "aaa";
		String s1 = "abcdefghijklmn";

		System.out.println(uniqueCharacters1(s) + " " + uniqueCharacters2(s)
			+ " " + uniqueCharacters3(s));
	
		System.out.println(reverseString(s1) + " " + reverseString2(s1));
		String s2 = reverseString(s1);

		System.out.println(isPermutation(s1, s2) + " " + isPermutation2(s1, s2));

		String s3 = "this siht            ";
		char chars[] = s3.toCharArray();
		replace(chars, 9);
		System.out.println(new String(chars));
		System.out.println(compress(s) + " " + compress(s1));

		int image[][] = {{1, 2}, {3, 4}};
		rotate(image);
		printMatrix(image);

		int cleanmattrix[][] = {{1,2,0,5,6}, {1,2,3,4,0}};
		printMatrix(cleanmattrix);
		clean(cleanmattrix);
		printMatrix(cleanmattrix);

		PPP("aa");
		PPP(""+isSubstring("aa", "aaa"));
		PPP(""+isStringRotate("isthis", "thisis"));
	}

	public static void printMatrix(int[][] input) {
		int N = input.length;
		int M = input[0].length;

		for (int i = 0; i < N; i++) {
			for (int j = 0; j < M; j++)
				System.out.print(input[i][j] + " "); 
			System.out.println("");
		}
	}
	public static void PPP(String s) {
		System.out.println(s);
	}

}