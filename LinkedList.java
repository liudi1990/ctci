public class LinkedList<E> {
	Node<E> head;
	public LinkedList() {
		this.head = new Node<E>();
	}

	public void add(E value) {
		Node p = head;
		while (p.next != null) {
			p = p.next;
		}
		p.next = new Node<E>();
		p.next.value = value;		
	}

	public void print() {
		Node p = head;
		while (p.next != null) {
			System.out.println(p.next.value);
			p = p.next;
		}
	}
}