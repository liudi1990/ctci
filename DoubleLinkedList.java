import java.util.*;

public class DoubleLinkedList<E> implements Iterable{
	public Node head;
	public Node tail;

	public class Node {
		E value;
		Node prev;
		Node next;
	}

	public DoubleLinkedList() {
		this.head = new Node();
		this.tail = new Node();

	  	this.head.next = this.tail;
	  	this.tail.prev = this.head;
	}

	public void addTail(E value) {
		Node newNode = new Node();
		newNode.value = value;
		newNode.next = tail;
		newNode.prev = tail.prev;
		newNode.next.prev = newNode;
		newNode.prev.next = newNode;
	}

	public void addHead(E value) {
		Node newNode = new Node();
		newNode.value = value;
		newNode.next = head.next;
		newNode.prev = head;
		newNode.next.prev = newNode;
		newNode.prev.next = newNode;
	}

	public Iterator<E> iterator() {
		return new myIterator();
	}

	public class myIterator implements Iterator<E> {
		public Node current;

		public myIterator() {
			this.current = DoubleLinkedList.this.head;
		}

		public boolean hasNext() {
			return this.current.next != DoubleLinkedList.this.tail;
		}

		public E next() {
			if (!hasNext()) return null;
			current = current.next;
			return current.value;
		}
	}
}