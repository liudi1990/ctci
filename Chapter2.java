import java.util.*;
public class Chapter2 {
//--------------------- Question 1 ------------------------//
	public static ListNode removeDuplicate(ListNode list) {
		HashSet<Integer> set = new HashSet<Integer>();

		ListNode dummy = new ListNode(1);
		dummy.next = list;
		
		ListNode p = dummy;
		while (p.next != null) {
			if (set.contains(p.next.val)) {
				p.next = p.next.next;
			} else {
				set.add(p.next.val);
				p = p.next;
			}
		}
		return dummy.next;
	}	
	// If no buffer allowed!

	public static ListNode removeDuplicate2(ListNode list) {
		ListNode dummy = new ListNode(0);
		dummy.next = list;
		ListNode p = dummy;

		while (p != null && p.next != null) {
			ListNode p2 = p.next;

			while (p2 != null && p2.next != null) {
				if (p2.next.val != p.next.val) {
					p2 = p2.next;
				} else {
					p2.next = p2.next.next;
				}
			}
			p = p.next;
		}
		
		return dummy.next;
	}
//--------------------- Question 2 ------------------------//

	public static int kthFromLast(int k, ListNode list) {
		if (list == null) return -1;

		ListNode p1 = list;
		ListNode p2 = list;
		for (int i = 0; i < k; i++) {
			p2 = p2.next;
			if (p2 == null) {
				return -1;
			}
		}

		while (p2.next != null) {
			p2 = p2.next;
			p1 = p1.next;
		}
		return p1.val;
	}

//--------------------- Question 3 ------------------------//
	// By default, we will delete (N)/2 the element in list, if N is even
	public static ListNode middleList(ListNode list) {
		if (list == null || list.next == null) return null;

		ListNode dummy = new ListNode(1);
		dummy.next = list;
		ListNode p1 = dummy;
		ListNode p2 = dummy;

		while (p2 != null && p2.next != null) {
			p2 = p2.next.next;
			if (p2 == null || p2.next == null) {
				break;
			}
			p1  = p1.next;
		}
		p1.next = p1.next.next;
		return dummy.next;
	}
//--------------------- Question 4 ------------------------//
	public static ListNode partition(ListNode list, int k) {
		if (list == null || list.next == null) return list;

		ListNode dummy = new ListNode(0);
		dummy.next = list;
		ListNode small = new ListNode (0);
		ListNode big = new ListNode(0);

		ListNode p = dummy;
		ListNode p1 = small;
		ListNode p2 = big;

		while (p.next != null) {
			ListNode temp = p.next;
			p.next = p.next.next;
			temp.next = null;
			if (temp.val < k) {
				p1.next = temp;
				p1 = p1.next;
			} else {
				p2.next = temp;
				p2 = p2.next;
			}
		}
		p1.next = big.next;
		return small.next;
	}
//--------------------- Question 5 ------------------------//
	// Note:
	/* corner cases: a. after the last computation, we have 1 in carrier,
					 b. if both of them are 0
		About follow up question:
			1. we could just reverse the input list
			2. we could keep the carrier and store it with reverse order
				and count these carriers & reverse the result list again
				
		Both ways are tedious to implement, so...... 
					 */

	public static ListNode addingReverse(ListNode l1, ListNode l2) {
		int carrier = 0;

		ListNode dummy = new ListNode(0);

		ListNode p1 = l1;
		ListNode p2 = l2;
		ListNode p = dummy;
		while (p1 != null || p2 != null) {
			int current = 0;
			if (p1 != null) {
				current += p1.val;
				p1 = p1.next;
			}

			if (p2 != null) {
				current += p2.val;
				p2 = p2.next;
			}
			current += carrier;
			carrier = current / 10;
			current %= 10;
			ListNode temp = new ListNode(current);

			temp.next = p.next;
			p.next = temp;
		}
		if (carrier != 0) {
			ListNode newhead = new ListNode(carrier);
			newhead.next = dummy.next;
			return newhead;
		}
		if (dummy.next == null) {
			return new ListNode(0);
		}
		return dummy.next;
	}

//--------------------- Question 6 ------------------------//
	// Leetcode 
	public static ListNode detectCycle(ListNode head) {
	    ListNode dummy = new ListNode(0);
	    dummy.next = head;
	    
	    ListNode p1 = dummy;
	    ListNode p2 = dummy;
	    
	    while (p2 != null) {
	        p2 = p2.next;
	        if (p2 == null) return null;
	        p2 = p2.next;
	        if (p2 == null) return null;
	        p1 = p1.next;
	        if (p2 == p1)
	            break;
	    }
	    
	    p2 = dummy;
	    while (p2 != p1) {
	        p2 = p2.next;
	        p1 = p1.next;
	    }
	    return p2;
	}

//--------------------- Question 7 ------------------------//
	// Little note: we could just search half of this list.
	public static boolean checkPalidrome(ListNode list) {
		if (list == null) return false;

		ListNode dummy = new ListNode(0);
		ListNode p = list;
		while (p != null) {
			ListNode temp = new ListNode(p.val);
			temp.next = dummy.next;
			dummy.next = temp;
			p = p.next;
		}
		p = dummy.next;
		ListNode p1 = list;

		while (p != null) {
			if (p.val != p1.val) return false;
			p = p.next;
			p1 = p1.next;
		}
		return true;
	}
	public static void main(String[] args) {

		P("This is the start of Chapter2");
		
		ListNode head = create(10, 1);
		head.p();
		removeDuplicate(head).p();
		ListNode head2 = create(10, 1);
		removeDuplicate2(head2).p();

		ListNode l = create(10, 1);
		P("" + kthFromLast(200, l) + " " + kthFromLast(2, l));
		ListNode l1 = create(2, 1);
		middleList(l1).p();
		middleList(create(5,1)).p();
		partition(create(10,1), 7).p();    
		create(4,1).p();
		create(3,1).p();
		addingReverse(create(4,1), create(3,1)).p();
		addingReverse(new ListNode(0), new ListNode(0)).p();
		P(""+checkPalidrome(create(2,1)));
		P("This is the end!\n");
	}

	// Support Functions
	public static void P(String s) {
		System.out.println(s);
	}

	public static ListNode create(int length, int seed) {
		Random rand = new Random(seed);
		ListNode dummy = new ListNode(0);
		ListNode p = dummy;

		for (int i  = 0; i < length; i++) {
			p.next = new ListNode(rand.nextInt(10) + 1);
			p = p.next;
		}
		return dummy.next;
	}
}