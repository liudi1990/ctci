public class ListNode {
	int val;
	ListNode next;

	public ListNode(int val) {
		this.val = val;
	}
	public void p() {
		ListNode p = next;
		System.out.print(val);

		while (p != null) {
			System.out.print(" "+p.val);
			p = p.next;
		}
		System.out.println("");
	}	
}